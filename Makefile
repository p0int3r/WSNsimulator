# Using the mpicc compiler with compiler
# debugging enabled and optimization level 2.
CC 		= mpicc
CFLAGS	= -Wall -O2


# The source files to be compiled,
# the object codes to be generated
# and linked into the executable.
SRC = main.c wsn.c
OBJ = main.o wsn.o
EXE = simulation


# Default make command,compiles
# the source files into the 
# corresponding executable.
all: $(OBJ) Makefile
	$(CC) $(CFLAGS)   -o $(EXE) $(OBJ)


# The clean command removes the
# object files generated as well
# as the executable file.
clean:
	rm -f $(OBJ) $(EXE)


# The object files main.o and wsn.o
# should be re-generated everytime
# the header file wsn.h changes.
main.o: wsn.h Makefile
wsn.o:  wsn.h Makefile

