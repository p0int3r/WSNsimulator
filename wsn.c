/*
 * This file contains the definition
 * of procedures regarding the wireless
 * sensor network node data structure.
 *
 * @author: Endri Kastrati
 * @date:   18/04/2017
 *
 */



/*
 * Including the standard input-output library,
 * the standard utilities library,the standard
 * string manipulation library,the standard time
 * library,the standard unix library,the standard
 * system time library,the standard assert library
 * and the header file "wsn.h" that contains definitions
 * of datatypes and function prototypings regarding the
 * WSNnode data structure.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <assert.h>
#include "wsn.h"



/*
 * The static function WSNnode_getNbrs() takes two arguments
 * as parameters.The first one is a WSNnode_t data structure
 * and the second one is an MPI_Comm data structure.It figures
 * out the adjacent neighbors to the given reference node.This
 * function uses internally the MPI_Cart_rank() function of the
 * MPI library.
 *
 * @param:  WSNnode_t       *node
 * @param:  MPI_Comm        comm
 * @return: void
 *
 */

static void WSNnode_getNbrs(WSNnode_t *node,MPI_Comm comm)
{
    int n=15,m=4;           // The cartesian size of the grid, m x n.
    int coords[2];          // An array that stores x,y at 0,1.
    int dummy_rank;         // a dummy rank variable for the special cases.
    assert(node!=NULL);     // Making sure the given node data structure is not null.

    if (node->x==0 && node->y==0)
    {
        // Case 1:
        //
        //      If we are at cartesian position (0,0)
        //      Then the adjacent neighbors to the
        //      current reference node are the node at:
        //          
        //          n1: (0,1)
        //          n2: (0,2)
        //          n3: (1,0)
        //          n4: (1,1)
        //      
        //      We retrieve the rank values of each
        //      neighbor and assign it to the neighbors_t
        //      field of the node data structure.
        coords[0]=0; coords[1]=1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n1);
        coords[0]=0; coords[1]=2;
        MPI_Cart_rank(comm,coords,&node->nbrs.n2);
        coords[0]=1; coords[1]=0;
        MPI_Cart_rank(comm,coords,&node->nbrs.n3);
        coords[0]=1; coords[1]=1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->x==0 && node->y==(n-1))
    {
        // Case 2:
        //      
        //      If we are at cartesian position (0,n-1)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //
        //          n1: (0,n-2)
        //          n2: (1,n-2)
        //          n3: (1,n-1)
        //          n4: (2,n-1)
        //
        //      We retrieve the rank values of each
        //      neighbor and assign it to the neighbors_t
        //      field of the node data structure.
        coords[0]=0; coords[1]=(n-2);
        MPI_Cart_rank(comm,coords,&node->nbrs.n1);
        coords[0]=1; coords[1]=(n-2);
        MPI_Cart_rank(comm,coords,&node->nbrs.n2);
        coords[0]=1; coords[1]=(n-1);
        MPI_Cart_rank(comm,coords,&node->nbrs.n3);
        coords[0]=2; coords[1]=(n-1);
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->x==(m-1) && node->y==0)
    {
        // Case 3:
        //      
        //      If we are at cartesian position (m-1,0)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //          
        //          n1: (m-1,1)
        //          n2: (m-2,0)
        //          n3: (m-3,0)
        //          n4: (m-2,1)
        //
        //      We retrieve the rank values of each neighbor
        //      and assign it to the neighbors_t field of
        //      node data structure.
        coords[0]=m-1; coords[1]=1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n1);
        coords[0]=(m-2); coords[1]=0;
        MPI_Cart_rank(comm,coords,&node->nbrs.n2);
        coords[0]=(m-3); coords[1]=0;
        MPI_Cart_rank(comm,coords,&node->nbrs.n3);
        coords[0]=(m-2); coords[1]=1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->x==(m-1) && node->y==(n-1))
    {
        // Case 4:
        //      
        //      If we are at cartesian position (m-1,n-1)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //          
        //          n1: (m-1,n-2)
        //          n2: (m-1,n-3)
        //          n3: (m-2,n-1)
        //          n4: (m-2,n-2)
        //      
        //      We retrieve the rank values of each neighbor
        //      and assign it to the neighbors_t field of
        //      the node data structure.
        coords[0]=(m-1); coords[1]=(n-2);
        MPI_Cart_rank(comm,coords,&node->nbrs.n1);
        coords[0]=(m-1); coords[1]=(n-3);
        MPI_Cart_rank(comm,coords,&node->nbrs.n2);
        coords[0]=(m-2); coords[1]=(n-1);
        MPI_Cart_rank(comm,coords,&node->nbrs.n3);
        coords[0]=(m-2); coords[1]=(n-2);
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->x==0)
    {
        // Case 5:
        //      
        //      If we are at a cartesian position (0,_)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //          
        //          n1: down
        //          n2: left
        //          n3: right
        //          n4: down-left diagonal
        //
        //      We retrieve the rank values of each neighbor and
        //      assign it to the neighbors_t field of the node
        //      data structure.
        MPI_Cart_shift(comm,0,1,&dummy_rank,&node->nbrs.n1);
        MPI_Cart_shift(comm,1,1,&node->nbrs.n2,&node->nbrs.n3);
        coords[0]=node->x+1; coords[1]=node->y-1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->x==(m-1))
    {
        // Case 6:
        //      
        //      If we are at a cartesian position (m-1,_)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //          
        //          n1: up
        //          n2: left
        //          n3: right
        //          n4: top-right diagonal
        //
        //      We retrieve the rank values of each neighbor and
        //      assign it to the neighbors_t field of the node
        //      data structure.
        MPI_Cart_shift(comm,0,1,&node->nbrs.n1,&dummy_rank);
        MPI_Cart_shift(comm,1,1,&node->nbrs.n2,&node->nbrs.n3);
        coords[0]=node->x-1; coords[1]=node->y+1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->y==0)
    {
        // Case 7:
        //      
        //      If we are at a cartesian position (_,0)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //          
        //          n1: up
        //          n2: down
        //          n3: right
        //          n4: down-right diagonal
        //
        //      We retrieve the rank values of each neighbor and
        //      assign it to the neighbors_t field of the node
        //      data structure.
        MPI_Cart_shift(comm,0,1,&node->nbrs.n1,&node->nbrs.n2);
        MPI_Cart_shift(comm,1,1,&dummy_rank,&node->nbrs.n3);
        coords[1]=node->y+1; coords[0]=node->x+1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else if (node->y==(n-1))
    {
        // Case 8:
        //      
        //      If we are at a cartesian position (_,n-1)
        //      then the adjacent neighbors to the current
        //      reference node are the nodes at:
        //          
        //          n1: up
        //          n2: down
        //          n3: left
        //          n4: up-left diagonal
        //
        //      We retrieve the rank values of each neighbor and
        //      assign it to the neighbors_t field of the node
        //      data structure.
        MPI_Cart_shift(comm,0,1,&node->nbrs.n1,&node->nbrs.n2);
        MPI_Cart_shift(comm,1,1,&node->nbrs.n3,&dummy_rank);
        coords[1]=node->y-1; coords[0]=node->x-1;
        MPI_Cart_rank(comm,coords,&node->nbrs.n4);
    }
    else
    {
        // Case 9:
        //      
        //      For any other cartesian position (x,y) the
        //      adjacent neighbors to the current reference node
        //      are the nodes at:
        //          
        //          n1: up
        //          n2: down
        //          n3: left
        //          n4: right
        //
        //      We retrieve the rank values of each neighbor and
        //      assign it to the neighbors_t field of the node
        //      data structure.
        MPI_Cart_shift(comm,0,1,&node->nbrs.n1,&node->nbrs.n2);
        MPI_Cart_shift(comm,1,1,&node->nbrs.n3,&node->nbrs.n4);
    }

    return;
}





/*
 * The public function WSNnode_create() takes three arguments
 * as parameters.The first argument is the rank value of the
 * node we want to create.The second one, is the id value of
 * the root node (workstation).The final argument is a MPI_Comm
 * data structure that represents the cartesian grid world of 
 * the wireless sensor network.This function instantiates a new
 * WSNnode_t data structure by allocating memory for itself and
 * it'ss components and setting them to their default values.
 * Once the instantiation has been completed the function returns
 * the memory address of the newly created WSNnode_t data structure.
 *
 * @param:  int             rank
 * @param:  int             root
 * @param:  MPI_Comm        comm
 * @return: WSNnode_t       *
 *
 */

WSNnode_t *WSNnode_create(int rank,int root,MPI_Comm comm)
{
    // Variable declarations and
    // default instantiations.
    int i,coords[2];
    WSNnode_t *new_node=NULL;
    
    // Allocating memory for the WSNnode_t data
    // structure and checking if the returned memory
    // address does not equalt NULL.If it does then
    // the program execution terminates immediately.
    new_node=(WSNnode_t *)malloc(sizeof(*new_node));
    assert(new_node!=NULL);
    
    // Instantiating the fields of the 
    // node data structure to their 
    // corresponding values.Once everything
    // has been completed the memory address
    // to the newly allocated WSNnode_t data
    // structure is returned.
    new_node->id=rank;
    new_node->root=root;
    MPI_Cart_coords(comm,rank,2,coords);
    new_node->x=coords[0];
    new_node->y=coords[1];
    new_node->random_number=0;
    WSNnode_getNbrs(new_node,comm);
    for (i=0;i<4;i++) { new_node->nbrs_numbers[i]=0; }
    return new_node;
}




/*
 * The public function WSNnode_broadcast() take two arguments
 * as parameters.The first argument is a WSNnode_t data structure
 * and the second is a MPI_Comm data structure.This function sends
 * the randomly generated number of the current reference node to
 * it's adjacent neighbors in a non-blocking mode.
 *
 * @param:  WSNnode_t       *node
 * @param:  MPI_Comm        comm
 * @return: void
 *
 */

void WSNnode_broadcast(WSNnode_t *node,MPI_Comm comm)
{
    // Variable declarations and
    // default instantiations.
    int current_rank;
    static int tag=1;
    assert(node!=NULL);
    MPI_Request request;
    MPI_Comm_rank(comm,&current_rank);

    // Because the currrent WSNnode_t data
    // structure has generated a random number,
    // that number is send in an asynchronous 
    // mode to it's adjacent neighbors.Once it
    // has completed the broadcasting to it's
    // neighbors the static variable tag is
    // incremented by one.
    if (node->id==current_rank)
    {
        MPI_Isend(&node->random_number,1,MPI_INT,node->nbrs.n1,tag,comm,&request);
        MPI_Isend(&node->random_number,1,MPI_INT,node->nbrs.n2,tag,comm,&request);
        MPI_Isend(&node->random_number,1,MPI_INT,node->nbrs.n3,tag,comm,&request);
        MPI_Isend(&node->random_number,1,MPI_INT,node->nbrs.n4,tag,comm,&request);
        tag++;
    }

    return;
}




/*
 * The public function WSNnode_gather() takes two arguments
 * as parameters.The first argument is a WSNnode_t data structure
 * and the second argument is a MPI_Comm data structure.This function
 * gathers the randomly generated numbers from it's adjacent neighbors
 * in a non-blocking mode, and stores them into the it's neighbors_numbers 
 * component which is just an array of integers.
 *
 * @param:  WSNnode_t       *node
 * @param:  MPI_Comm        comm
 * @return: void
 *
 */

void WSNnode_gather(WSNnode_t *node,MPI_Comm comm)
{
    // Variable declaration and
    // default instantiation.
    int current_rank;
    static int tag=1;
    assert(node!=NULL);
    MPI_Request request;
    MPI_Comm_rank(comm,&current_rank);
    
    // At this moment the neighbors of
    // the current reference node have
    // also generated their random numbers.
    // Hence, we receive those values from
    // the neighbors and store each one to
    // it's corresponding cell.Once the gather
    // from neighbors has been completed the
    // static variable tag is incremented by one.
    if (node->id==current_rank)
    {
        MPI_Irecv(&node->nbrs_numbers[0],1,MPI_INT,node->nbrs.n1,tag,comm,&request);
        MPI_Irecv(&node->nbrs_numbers[1],1,MPI_INT,node->nbrs.n2,tag,comm,&request);
        MPI_Irecv(&node->nbrs_numbers[2],1,MPI_INT,node->nbrs.n3,tag,comm,&request);
        MPI_Irecv(&node->nbrs_numbers[3],1,MPI_INT,node->nbrs.n4,tag,comm,&request);
        tag++;
    }

    return;
}




/*
 * The public function WSNnode_isEvent() takes only one 
 * argument as a parameter,namely a WSNnode_t data structure
 * and checks whether it's random number and it's neighbors
 * numbers are above a certain threshold.If so it returns
 * true otherwise it returns false.
 *
 * @param:  WSNnode_t   *node
 * @return: int
 *
 */

int WSNnode_isEvent(WSNnode_t *node)
{
    // If the neighbors including the
    // reference node have detected
    // an event,namely the random numbers
    // are all above 5 then we return TRUE.
    // otherwise we return FALSE.
    assert(node!=NULL); int i;
    if (node->random_number<5) { return FALSE; }
    for (i=0;i<4;i++) { if (node->nbrs_numbers[i]<5) { return FALSE; } }
    return TRUE;
}



/*
 * The static function timestamp() takes two arguments
 * as parameters.The first argument is a pointer to
 * a buffer of characters and the second is the size
 * of the buffer.This function calculates the current
 * time and assigns  a string that has the following
 * format "yyyy:mm:dd hh:mm:ss.ms" to the given buffer.
 *
 * @param:  char    *buffer
 * @param:  int     n
 * @return: void
 *
 */

static void timestamp(char *buffer,int n)
{
    time_t current_time;
    struct timeval time_value;
    struct tm *chronos;
    char chrono_buffer[64];
    gettimeofday(&time_value,NULL);
    current_time=time_value.tv_sec;
    chronos=localtime(&current_time);
    strftime(chrono_buffer,sizeof(chrono_buffer),"%Y-%m-%d %H:%M:%S",chronos);
    snprintf(buffer,n,"%s.%06ld",chrono_buffer,time_value.tv_usec);
    return;
}




/*
 * The public function WSNnode_sendStation() takes two
 * arguments as parameters.The first argument is an
 * WSNnode_t data structure and the second a MPI_Comm
 * data structure.This function copies the components
 * of the given node into an array of integers including
 * the current time stamp.Once the copying has been
 * completed the buffer is sent to the workstation
 * in a non-blocking mode.
 *
 * @param:  WSNnode_t       *node
 * @param:  MPI_Comm        comm
 * @return: void
 *
 */

void WSNnode_sendStation(WSNnode_t *node,MPI_Comm comm)
{
    // Variable declaration and
    // default instantiation.
    int n=64,i,length;
    static int tag=1;
    assert(node!=NULL);
    int buffer[100];
    char time_string[64];
    MPI_Request request;
    assert(node!=NULL);
    
    // Recording the time that
    // the event was detected.
    timestamp(time_string,n);
    length=strlen(time_string)+1;
    
    // Copying the entire node data structure
    // into the buffer array and sending it
    // asynchronously to the workstation.
    buffer[0]=node->root; buffer[1]=node->id;
    buffer[2]=node->x; buffer[3]=node->y;
    buffer[4]=node->random_number;
    for (i=0;i<4;i++) { buffer[5+i]=node->nbrs_numbers[i]; }
    buffer[9]=node->nbrs.n1; buffer[10]=node->nbrs.n2;
    buffer[11]=node->nbrs.n3; buffer[12]=node->nbrs.n4;
    strcpy((char *)(buffer+13),time_string); 
    MPI_Isend(buffer,12+length,MPI_INT,node->root,tag,comm,&request);
    tag++;
    return;
}




/*
 * The public function WSNroot_station() takes three
 * arguments as parameters,namely the number of micro
 * seconds the workstation should sleep,the name of the
 * file the output should be written and an MPI_Comm
 * data structure.This function listens for events
 * that have or potentially are being send from
 * the nodes in the grid world.Every time it receives
 * data it records the time the message was received,
 * it formats it and prints it as a json data structure.
 * If the workstation receives 60 messages that have a
 * tag equal to the value of FALSE,the workstation stops
 * listening and returns the number of events it detected.
 *
 * @param:  int         mus
 * @param:  char        *outfile
 * @param:  MPI_Comm    global
 * @return: int
 *
 */

int WSNroot_station(int mus,char *outfile,MPI_Comm global)
{
    // Variable declarations and
    // default instantiations.
    int buffer[100];
    int count,n=64,i;
    int num_falses=0,total;
    int num_events=0,flag;
    MPI_Status status;
    MPI_Request request;
    char time_sent[64];
    char time_received[64];
    char simulation_start[64];
    char simulation_end[64];
    FILE *stream=NULL;
    WSNnode_t temp_node;
    MPI_Comm_size(global,&total);

    // Checking if the user has supplied
    // a specific filename for the output
    // to written in.
    if (strcmp(outfile,"stdout")==0) 
    { 
        // In case the user has not,or has
        // given the string "stdout" then
        // we print into the standard out
        // stream.
        stream=stdout; 
        flag=0;
    }
    else 
    { 
        // Otherwise,we open the corresponding
        // stream with write mode on.
        stream=fopen(outfile,"w");
        flag=1;
    }
    
    // Record the time that the workstation
    // starts listening for events, and begin
    // listening for events in the wireless
    // sensor network.
    timestamp(simulation_start,n);
    while (TRUE)
    {
        // Because we are inside an infinite loop
        // we check with every iteration if the
        // total number of false tags has been
        // received by the workstation.It it has
        // we exit the loop,otherwise we listen
        // for events and sleep based on the supplied
        // number of micro seconds.Once we receive an
        // event we record the time it was received by
        // the workstation.
        if (num_falses==total-1) { break; }
        MPI_Irecv(buffer,100,MPI_INT,MPI_ANY_SOURCE,MPI_ANY_TAG,global,&request);
        usleep(mus); MPI_Wait(&request,&status); 
        MPI_Get_count(&status, MPI_INT, &count);
        timestamp(time_received,n);
        
        // If we did not receive a dummy event from
        // the network nodes,read the data from the
        // received buffer and store them into their
        // corresponding fields of the WSNnode_t data
        // structure.Once completed write the data into
        // the specified stream.
        if (status.MPI_TAG!=FALSE)
        {
            num_events++;
            temp_node.root=buffer[0]; temp_node.id=buffer[1];
            temp_node.x=buffer[2]; temp_node.y=buffer[3];
            temp_node.random_number=buffer[4];
            for (i=0;i<4;i++) { temp_node.nbrs_numbers[i]=buffer[5+i]; }
            temp_node.nbrs.n1=buffer[9]; temp_node.nbrs.n2=buffer[10];
            temp_node.nbrs.n3=buffer[11]; temp_node.nbrs.n4=buffer[12];
            strcpy(time_sent,(char *)(buffer+13));
            fprintf(stream,"{\"received\":\"%s\",\"sent\":\"%s\",\"node\":",time_received,time_sent);
            WSNnode_print(&temp_node,stream); fprintf(stream,"}\n");
        }
        else { num_falses++; }
    }
    
    // Record the time that the workstation terminates
    // the listening of events and write into the specified
    // stream the time the simulation started,ended as well
    // as the total number of events detected by the workstation
    // during the simulation.
    timestamp(simulation_end,n);
    fprintf(stream,"{\"started\":\"%s\",\"ended\":\"%s\",\"events\":%d}\n",
        simulation_start,simulation_end,num_events);
    if (flag==1) { fclose(stream); }
    return num_events;
}





/*
 * The public function WSNnode_print() takes two
 * arguments as a parameters,namely an WSNnode_t
 * data structure and a FILE data structure and
 * writes the given node as json into the given 
 * file stream.
 *
 * @param:  WSNnode_t   *node
 * @param:  FILE        *stream
 * @return: void
 *
 */

void WSNnode_print(WSNnode_t *node,FILE *stream)
{
    assert(node!=NULL);
    fprintf(stream,"{\"refID\":%d,\"refCoords\":[%d,%d],\"nbrsID\":[%d,%d,%d,%d]}",
        node->id,node->x,node->y,node->nbrs.n1,node->nbrs.n2,node->nbrs.n3,node->nbrs.n4);
    fflush(stdout);
    return;
}




/*
 * The public method WSNnode_free() takes only one argument
 * as parameter,namely a WSNnode_t data structure and deallocates
 * the memory block associated with it and it's components.
 *
 * @param:  WSNnode_t       *node
 * @return: void
 *
 */

void WSNnode_free(WSNnode_t *node)
{
    assert(node!=NULL);
    free(node); node=NULL;
    return;
}



/*
 * The public method WSNgrid_simulate() takes four arguments
 * as parameters.The first argument is the total number of
 * simulations to be executed.The second one is the id value
 * of the root processor.The last two are MPI_Comm data structures,
 * that is the global MPI world and the grid world.This function
 * simulates a Wireless sensor network.Every node in the grid can
 * detect and confirm events with it's adjacent neighboring nodes.
 * If an event is detected and confirmed it is send to the workstation
 * in a non-blocking mode.After n simulations all nodes in the grid
 * send a message to the workstation with the tag equal to the value
 * of FALSE.Once that process is completed all memory blocks associated
 * with the current node data structure are deallocated.
 *
 * @param:  int         n
 * @param:  int         root
 * @param:  MPI_Comm    global
 * @param:  MPI_Comm    comm
 * @return: void
 *
 */

void WSNgrid_simulate(int n,int root,MPI_Comm global,MPI_Comm comm)
{
    time_t seed;                    // The address of which will be used as a RNG seed.
    int period[2]={0,0};            // No cyclic mode in either dimension.
    int flag=0;                     // A flag variable that can be either TRUE or FALSE.
    int rank;                       // The rank id of the current processor.
    int size;                       // The total number of nodes in the grid world.
    int dummy;                      // A dummy variable, to be send at the end of the simulation.
    int reorder=TRUE;               // Reordering of the ranks set to TRUE (for the grid world).
    int dimensions[2]={4,15};       // The dimensions of the grid world, namely 4 x 15.
    MPI_Comm grid_world;            // The grid world data structure.
    MPI_Request request;            // A request data structure to be used at the end of the simulation.
    WSNnode_t *node=NULL;           // The WSnnode_t data structure for the current process node.
    

    // Retrieving the size of the given
    // communicator as well as the rank
    // of the current process.We continue
    // with the creation of acommuncator
    // that represents the grid world.
    //
    // After the creation of the grid,we
    // seed the random  number generator
    // for each processor and generate
    // it's corresponding WSNnode_t data
    // structure.
    MPI_Comm_size(comm,&size);
    MPI_Comm_rank(comm,&rank);
    MPI_Cart_create(comm,2,dimensions,period,reorder,&grid_world);
    srand((unsigned )time(&seed)+rank*size);
    node=WSNnode_create(rank,root,grid_world);
    

    // We use loop iterations to represent 
    // discrete events.The number of iterations
    // is given as an argument.
    for (int i=0;i<n;i++)
    {

        // All process nodes within the
        // grid world simultaneously generate
        // a random number between 0 and 10.
        node->random_number=rand()%10;

        // Once the RNGs have been completed
        // all nodes broadcast their random
        // numbers to their adjacent nodes.
        WSNnode_broadcast(node,grid_world);

        // We wait for all nodes to broadcast
        // their values to their neighbors.
        MPI_Barrier(grid_world);

        // Once broadcasting has been completed,
        // all nodes gather the random numbers
        // from their adjacent neighbors.
        WSNnode_gather(node,grid_world);

        // Checking if the current node and
        // it's adjacent neighbors have detected
        // an event.That is if all adjacent nodes
        // have a random number above 5,an event
        // has been triggered, otherwise false
        // positives.
        flag=WSNnode_isEvent(node);
    
        if (flag==TRUE)
        {
            // If case of event detection,the
            // current reference node sends data
            // to the workstation.
            WSNnode_sendStation(node,global);
        }
    }

    // Once the number of simulations has been completed
    // We send a dummy message to the workstation with
    // the tag equal to FALSE and deallocate all memory
    // associated with the current node data structure.
    MPI_Isend(&dummy,1,MPI_INT,root,FALSE,global,&request);
    WSNnode_free(node); node=NULL;
    return;
}

