/*
 * This file contains datatype definitions
 * and function prototyping regarding the
 * abstract concept of a distributed wireless
 * sensor network.
 *
 * @author: Endri Kastrati
 * @date:   18/04/2017
 *
 */



/*
 * Using include guards to check if the
 * wsn.h header file has been included
 * at least once.If it hasn't,the compiler
 * copy-pastes everything into the file that
 * is including it.If the file on the other
 * hand has been included the compiler skips
 * the contents entirely.
 *
 */

#ifndef WSN_H
#define WSN_H




/*
 * Including the message passing
 * interface library that gives
 * a programmer explicit control
 * over interprocess communication.
 *
 */

#include <mpi.h>




/*
 * Defining two macro constants that
 * represent the boolean values true
 * and false.Everytime the compiler
 * encounters the macro TRUE or FALSE
 * it replaces it with 1 or 0.
 *
 */

#define TRUE    1
#define FALSE   0





/*
 * Defining a new data structure that represents
 * the nodes adjacent to a reference node.More
 * specifically the neighboring nodes n1,n2,n3
 * and n4.The components must be assigned rank 
 * values of the neighboring processes.
 *
 */

typedef struct
{
    int     n1;
    int     n2;
    int     n3;
    int     n4;
} neighbors_t;



/*
 * Defining a new data structure that represents
 * the abstract concept of a node in a wireless
 * sensor network.It has as fields the id of the
 * root,it's own id,it's cartesian coordinates in
 * the grid world,a randomly generated number,the
 * randomly generated numbers of it's adjacent nodes,
 * and last but not least a neighbors_t data structure.
 *
 */

typedef struct
{
    int             root;
    int             id;
    int             x,y;
    int             random_number;
    int             nbrs_numbers[4];
    neighbors_t     nbrs;
} WSNnode_t;




/*
 * Function prototyping of procedures regarding
 * the WSN node such as create,broadcast,gather etc..
 *
 */

WSNnode_t       *WSNnode_create(int rank,int root,MPI_Comm comm);
void            WSNnode_broadcast(WSNnode_t *node,MPI_Comm comm);
void            WSNnode_gather(WSNnode_t *node,MPI_Comm comm);
void            WSNnode_sendStation(WSNnode_t *node,MPI_Comm comm);
int             WSNnode_isEvent(WSNnode_t *node);
void            WSNnode_print(WSNnode_t *node,FILE *stream);
void            WSNnode_free(WSNnode_t *node);




/*
 * Function prototyping of procedures regarding
 * the WSN grid and root station.
 *
 */

void            WSNgrid_simulate(int n,int root,MPI_Comm global,MPI_Comm comm);
int             WSNroot_station(int mus,char *outfile,MPI_Comm global);



/*
 * Once everything has been copy-pasted
 * by the compiler and the macro WSN_H
 * has been defined, the wsn.h header
 * file will not be included more than once.
 *
 */

#endif
