/*
 * Main execution file,simulates
 * a wireless sensor network.
 * This file cotnains the coding
 * solution to Assignment 2.
 *
 * @author: Endri Kastrati
 * @date:   7/05/2017
 *
 */


/*
 * Including the standard input-output
 * library,the standard utilities library,
 * the standard assertions library,the
 * standard time library,the standard 
 * string manipulation library and the 
 * header file "wsn.h" that contains datatype
 * definitions and function prototyping
 * for procedures regarding the WSNnode_t
 * data structure.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "wsn.h"



/*
 * The function usage() takes no arguments as
 * parameters and prints usage information into
 * the standard error stream.
 *
 * @param:  void
 * @return: void
 *
 */

void usage(void)
{
    static const char *const content=
        "usage:\n"
        "   mpiexec -np 61 ./simulation [--n=<number>] [--mus-sleep=<number>] [--out-file=<string>]\n"
        "\n"
        "Available options:\n"
        "   --n=<number>            The number of simulations to be performed.\n"
        "   --ms-sleep=<number>     The number of microseconds the workstation should sleep.\n"
        "   --out-file=<string>     The name of the stream to store the output.\n"
        "   --help                  Prints this help and quits program execution.\n"
        "\n"
        "author: (c) Endri Kastrati, email: endriau@gmail.com\n";
    fprintf(stderr,"%s",content);
    return;
}








/*
 * Main execution function. Instantiates
 * all the necessary variables and data
 * structures, and initiates the message
 * passing interface world.Once every process
 * has obtained a copy of this program,the
 * wireless sensor network simulation begins.
 * All nodes at the same time randomly generate
 * a number which they broadcast at their adjcent
 * neighbors.Once broadcasting has been completed
 * all nodes gather the data sent from their
 * adjacent neighbors and check whether an event
 * has been triggered.In the case where an event
 * has been indeed detected and confirmed,the
 * reference node sends it to the root node.
 * The root node (workstation) awaits for any
 * incoming messages and prints them into the
 * standard output.
 *
 */

int main(int argc,char *argv[])
{
    int flag=0;                         // A variable that can hold either the values TRUE or FALSE.
    int rank;                           // The rank of the current process in the MPI_COMM_WORLD.
    int size;                           // The total number of processes in the MPI_COMM_WORLD.
    int master_root;                    // The rank id of the master root processor (workstation).
    int num_simulations;                // The total number of simulations to be executed.
    int micro_seconds;                  // The total number of microseconds the workstation should sleep.
    char *outfile=NULL;                 // The adress of the string containing the output filename.
    MPI_Comm node_world;                // The nodes world data structure.
    

    
    // Initializing the message passing interface
    // framework and retrieving the size of the
    // MPI world as well as the rank of the current
    // process in the MPI world.
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    master_root=0;
    


    if (rank==master_root)
    {
        // If the first argument equals to
        // the help parameter,then invoke
        // usage,abort and finalize MPI.
        if ( argc==2 && strcmp(argv[1],"--help")==0)
        {
            usage();
            MPI_Abort(MPI_COMM_WORLD,1);
            MPI_Finalize();
            return 0;
        }
        
        // If the size of the processes given
        // does not equal 61,invoke usage then
        // abort and finalize MPI.
        if (size!=61)
        {
            usage();
            MPI_Abort(MPI_COMM_WORLD,1);
            MPI_Finalize(); 
            return 0;
        }
    }

    
    // Checking if the user has supplied the number of
    // simulations he wishes to perform.
    if ((argc==2 || argc>2) && strstr(argv[1],"--n=")!=NULL)
    { 
        // If so convert that number into an
        // integer and copy it into the variable
        // num_simulations.
        num_simulations=atoi(&argv[1][4]);
    }
    else 
    { 
        // Otherwise set default simulation to 10.
        num_simulations=10;
    }

    
    // Checking if the user has supplied the number of
    // micro seconds the workstation can sleep.
    if ((argc==3 || argc>3) && strstr(argv[2],"--mus-sleep=")!=NULL)
    { 
        // If so convert that number into an
        // integer and copy it into the variable
        // micro_seconds.
        micro_seconds=atoi(&argv[2][12]);
    }
    else
    { 
        // Otherwise, set default 0 micro seconds
        // of sleep,(the workstation does not sleep).
        micro_seconds=0;
    }
    
    
    // Checking if the user has supplied the name of the
    // file that the output should be written into.
    if ((argc==4 || argc>4) && strstr(argv[3],"--out-file=")!=NULL)
    {
        // If so assign the address of the string
        // to the outfile pointer.
        outfile=&argv[3][11];
    }
    else 
    { 
        // Otherwise, outfile points to the string
        // "stdout",namely the standard output.
        outfile="stdout";
    }
    

    // Spliting the MPI world into a communicator
    // that contains everything besides the processor
    // with rank equal to the root master.
    flag=(rank==master_root ? 1 : 0);
    MPI_Comm_split(MPI_COMM_WORLD,flag,0,&node_world);

    
    if (rank==master_root)
    {

        // If the current processor rank is equal to
        // the root master,start listening for events.
        WSNroot_station(micro_seconds,outfile,MPI_COMM_WORLD);
    }
    else
    {
        // Otherwise initiate the grid world simulation.
        WSNgrid_simulate(num_simulations,master_root,MPI_COMM_WORLD,node_world);
    }
    

    // Free the communicator that contains
    // the slave processes and free resources
    // allocated by the MPI framework.
    MPI_Comm_free(&node_world);
    MPI_Finalize();
    return 0;
}

